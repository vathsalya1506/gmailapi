const express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    { google } = require('googleapis'),
    mongodb = require("mongodb").MongoClient;


const scope = ['https://mail.google.com/', 'https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/plus.profile.emails.read', 'https://www.googleapis.com/auth/gmail.modify', 'https://www.googleapis.com/auth/gmail.readonly', 'https://www.googleapis.com/auth/gmail.metadata'];
const cred = {
    clientID: "20185101493-i2rcjqs8e44ng840e5a7v1ltegk3qneu.apps.googleusercontent.com",
    clientSecret: "gYx7Xh9iK3JnI60uS23s8p7a",
    callbackURL: "http://localhost:3000/glogin"
};
const { client_id, client_secret, redirect_uris } = {
    "client_id": "20185101493-i2rcjqs8e44ng840e5a7v1ltegk3qneu.apps.googleusercontent.com",
    "client_secret": "gYx7Xh9iK3JnI60uS23s8p7a",
    "redirect_uris": ["http://localhost:3000/glogin"]
};

let mongDb = null;
const mongoURL = 'mongodb://localhost:27017/gmail';
let connectToMongo = new Promise(function (resolve,reject) {
    if(mongDb){
        resolve(mongDb);
    } else {
        mongodb.connect(mongoURL,function (err,conn) {
            if(err){
                reject(err);
            }else {
                mongDb = conn;
                resolve(conn);
            }

        })
    }
});




router.use(passport.initialize());
passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});
passport.use(new GoogleStrategy(cred,
    function (accessToken,refreshToken,profile,done) {
        const email = profile.emails[0].value;
        const obj = {};
        obj['access_token'] = accessToken;
        obj['refresh_token'] = refreshToken;
        let account = new Account(email,obj);
        account.updateTokens();
        account.assignTokens();
        account.getStartHistoryId();

    })
);

let Account = function (email,tokens) {
    this.auth = new google.auth.OAuth2(client_id,client_secret,redirect_uris[0]),
        this.email = email,
        this.tokens = tokens,
        this.assignTokens = function () {
            this.auth.setCredentials(this.tokens);
            let auth = this.auth;
            this.gmail  = google.gmail({'version':'v1',auth});
        },
        this.updateTokens = function () {
          this.assignTokens();
          new Promise((resolve,reject) => {
              this.auth.getAccessToken(function (err, newToken) {
                  if(err){
                      reject(err);
                  } else {
                      resolve(newToken);
                  }
              })
          }).then((tokens) => {
              this.tokens.access_token = tokens;
              this.auth.setCredentials(this.tokens);
              let auth = this.auth;
              this.gmail  = google.gmail({'version':'v1',auth});
              let email = this.email;
              let emailTokens = this.tokens;
              connectToMongo.then((db)=>{
                  db.collection('userTokens').findAndModify(
                      {"_id":email},
                      [],
                      emailTokens,
                      { upsert: true, new: true },function (err,res) {
                          if(err){
                              console.log(err);
                          }else {
                              console.log(res);
                          }
                          
                      }
                  )
                  this.getStartHistoryId();
              }).catch((err) => {
                  console.log('tokens update Error');
                  console.log(err.stack);
              });
          }).catch((err) => {
              console.log('getAccessToken Error');
              console.log(err.stack);
          });
        },
        this.getStartHistoryId = function () {
            this.gmail.users.getProfile({
                'userId':'me',
                'auth':this.auth
            }).then((profile) => {
                if(this.startHistoryId){
                    console.log('hello' + this.startHistoryId);
                } else {
                    console.log(profile.data);
                    this.startHistoryId = profile.data.historyId;
                    // this.startHistoryId = 467649;7514708
                    // this.startHistoryId = 7514708;
                    console.log(this.email,this.startHistoryId );
                        this.ListenForMessages();


                }
            }).catch((err) => {
                if(err.code==403){
                    this.updateTokens();
                }else{
                    console.log('getStartHistoryId error');
                    console.error(err.stack);
                }
            })
        },
        this.ListenForMessages = function () {
            console.log("[**]",this.email,this.startHistoryId );
            this.gmail.users.history.list({
                'userId': 'me',
                'startHistoryId': this.startHistoryId
            }).then((result) => {
                // console.log("[*]",result.data);

                if (Array.isArray(result.data.history)) {
                    console.log("[-]",this.email,this.startHistoryId)
                    this.startHistoryId = result.data.history[result.data.history.length - 1]["id"];
                    console.log("[+]",this.email,this.startHistoryId)
                    result.data.history.forEach((msg) => {
                        if (msg["messagesAdded"] !== undefined && msg["messagesAdded"] != null) {
                            this.getMessage(msg);
                        } else {
                            console.log(msg);
                        }
                    });
                    let timeout = setTimeout(()=>{
                        this.ListenForMessages();
                    },1000 *30)
                } else {
                    console.log("{*}No messages are added to ",this.email);
                    let timeout = setTimeout(() =>{
                        this.ListenForMessages();
                    },1000 *30)
                }
            }).catch((err) => {
                if(err.code==403){
                    this.updateTokens();
                }else{
                    console.log('ListenForMessages error');
                    console.log("{*err}",this.email,this.startHistoryId);
                    console.error(err.stack);
                }
            })
        },
        this.getMessage = function (element) {
            let auth=this.auth;
            this.gmail.users.messages.get({
                'userId': 'me',
                'auth': auth,
                'id': element["messagesAdded"][0]["message"]["id"],
                'format': 'metadata',
                'metadataHeaders': ["To", "From", "Subject","X-ReqIbr","Date","Message-ID"]
            }).then((result)=>{
                // console.log(result.data);
                // console.log(JSON.stringify(result.data.payload.headers));
                var m = {};
                var insertInDB = false;
                m["_id"] = result.data.id;
                m["email"]=this.email;
                m["Date"]=new Date(m["Date"]).getTime();
                m.label = result.data.labelIds;
                result.data.payload.headers.forEach((header) => {
                    m[header.name] = header.value;
                    if(header.name === 'Message-ID') {
                        var splitArray = header.value.split('_');
                        if(splitArray.length == 10) {
                            insertInDB = true;
                            m['campId'] = splitArray[2];
                            m['domainId'] = splitArray[3];
                            m["Project"] = "way2target";
                            console.log(splitArray);
                        } else if(splitArray.length == 12)  {
                            insertInDB = true;
                            m['campId'] = splitArray[2];
                            m['domainId'] = splitArray[3];
                            m["Project"] = "way2target_selfservice";
                        } else if(splitArray.length == 8){
                            insertInDB = true;
                            m['campId'] = splitArray[2];
                            m['domainId'] = splitArray[6];
                            m["Project"] = "P_Labs";
                        }
                    }
                });
                m["sender"] = m["From"].split("<")[0].trim();
                if(insertInDB) {
                    connectToMongo.then((db)=>{
                        db.collection("emailTestData").insert(m).then((result)=>{
                            console.log(result.result);
                        }).catch((err)=>{

                            if(err.code==11000){
                                console.error("Already Exited");
                                console.error(m)
                            }else{
                                console.error(err.stack);
                            }

                        })
                    }).catch((err)=>{
                        console.log(err.stack);
                    })
                } else {
                    console.log("not our mail id");
                }

            }).catch((err)=>{
                if(err.code==403){
                    this.updateTokens();
                }else{
                    console.log('getMessage error');
                    console.error(err.stack);
                }
            })
        };


};

connectToMongo.then(function (db) {
    db.collection("userTokens").find({}).toArray(function (err, userList) {
        if (err) {
            console.log(err);
        } else {
            console.log('by using mongo tokens');
            userList.forEach((user,index) => {
                setTimeout(function () {
                    let gmail = new Account(user["_id"], user);
                    // gmail.updateTokens();
                    gmail.assignTokens();
                    gmail.getStartHistoryId();

                    console.log(gmail.email);
                },index*5*1000);
            });
        }
    })
});




/* GET home page. */
router.get('/', passport.authenticate('google', { scope: scope, accessType: 'offline', prompt: 'consent' }));

/*Success callback */
router.get('/glogin', passport.authenticate('google', { failureRedirect: '/' }),
    function (req, res) {
        // res.redirect("/thanks")
        res.render("index", { "title": "Welcome to Way2Target" })
    });
module.exports = router;
