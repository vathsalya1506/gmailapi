const Imap = require('imap'),
    inspect = require('util').inspect,
    mongodb = require("mongodb").MongoClient,
    fs = require('fs');

let mongDb = null;
const mongoURL = 'mongodb://localhost:27017/gmail';
let connectToMongo = new Promise(function (resolve,reject) {
    if(mongDb){
        resolve(mongDb);
    } else {
        mongodb.connect(mongoURL,function (err,conn) {
            if(err){
                reject(err);
            }else {
                mongDb = conn;
                resolve(conn);
            }

        })
    }
});
function getUsersList() {
    connectToMongo.then((db)=>{
        db.collection("users_yahoo").find({},{_id:0}).toArray().then((result)=>{
            console.log(result);
            result.forEach((el,index)=>{
                setTimeout(function () {
                    ReadUser(el);
                },index*30*1000);
            })
        }).catch((err)=>{
            console.error(err.stack);
        })
    }).catch((err)=>{
        console.log(err);
    });
}
getUsersList();

process.on('uncaughtException',function (err) {
    console.log(err.stack);
    if(err.Error == 'Not authenticated') {
        console.log('not authenticated error');
        let options = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' ,hour12:true,hour:'2-digit',minute:'2-digit'};
        let dateTime = new Date(new Date().getTime()).toLocaleDateString('en-GB', options);
        let file = '\n' + err.stack + '\n'+ 'Not authenticated ' + dateTime + '\n' ;
        fs.appendFile('err-log.txt',file,(error) => {
            console.log(error);
        });
        getUsersList();
    } else if(err.toString().indexOf('Not authenticated') > -1) {
        console.log('err.toString');
        let options = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' ,hour12:true,hour:'2-digit',minute:'2-digit'};
        let dateTime = new Date(new Date().getTime()).toLocaleDateString('en-GB', options);
        let file = '\n' + err.stack + '\n'+ 'Not authenticated index of ' + dateTime + '\n' ;
        fs.appendFile('err-log.txt',file,(error) => {
            console.log(error);
        });
        getUsersList();
    } else {
        console.log('else condition ::::: ',JSON.stringify(err));
    }

});
function ReadUser(userDetails){
    let imap = new Imap({
        user:userDetails.user,
        password:userDetails.password,
        host: 'imap.mail.yahoo.com',
        port: 993,
        tls: true,
        authTimeout: 5000
    });
    function openInbox(folder,cb) {
        if(imap.state != 'authenticated') {
            imap.connect();
        }
        imap.openBox(folder, true, cb);
        // imap.openBox('Bulk Mail', true, cb);
    }
    let seqNum = {};
    let callBack = function(err, box) {
        if (err) {
            console.log(err);
            let boxDetails = box.name + ' ******* '+ userDetails.user;
            let options = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' ,hour12:true,hour:'2-digit',minute:'2-digit'};
            let dateTime = new Date(new Date().getTime()).toLocaleDateString('en-GB', options);
            let file = '\n' + err + '\n'+ dateTime + '\n' + boxDetails + '\n' +imap.state + '\n';
            fs.appendFile('err-log.txt',file,(error) => {
                console.log(error);
            });
        }
        else {
            if(!seqNum[box.name]){
                // seqNum[box.name] = box.messages.total;
                if(box.messages.total>3){
                    seqNum[box.name] = (box.messages.total)-3;
                } else {
                    seqNum[box.name] = box.messages.total;
                }
            }
            console.log(userDetails.user,'*****',box.name,'******',seqNum[box.name]);
            if(imap.state != 'authenticated') {
                imap.connect();
            }
            setTimeout(function () {
                var f = imap.seq.fetch((seqNum[box.name] + 1) + ':*', {
                    markSeen: false,
                    bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE MESSAGE-ID)']
                });
                f.on('message', function(msg, seqno) {
                    seqNum[box.name]  = seqno;
                    var prefix = '(#' + seqno + ') ';
                    msg.on('body', function(stream, info) {
                        var buffer = '', count = 0;
                        stream.on('data', function(chunk) {
                            count += chunk.length;
                            buffer += chunk.toString('utf8');
                        });
                        stream.once('end', function() {
                            if (info.which !== 'TEXT'){
                                console.log(prefix + 'Parsed header: %s', JSON.stringify(Imap.parseHeader(buffer)));
                                let headers = Imap.parseHeader(buffer);
                                let m = {};
                                let insertInDB = false;
                                m["seqNo"] = seqno;
                                m["email"]=userDetails.user;
                                m["Time"]=parseInt(new Date(headers.date[0]).getTime()/1000);
                                m["Date"]= new Date(new Date(headers.date[0]).getTime());
                                let options = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' ,hour12:true,hour:'2-digit',minute:'2-digit'};
                                m["Date"] = m["Date"].toLocaleDateString('en-GB', options);
                                m["From"]= headers.from[0];
                                m["To"]= headers.to[0];
                                m["Subject"]= headers.subject[0];
                                m.label = box.name;
                                if(headers['message-id'])
                                    m['Message-ID'] = headers['message-id'][0];
                                if(m['Message-ID']) {
                                    var splitArray = m['Message-ID'].split('_');
                                    if(splitArray.length == 10) {
                                        insertInDB = true;
                                        m['campId'] = parseInt(splitArray[2]);
                                        m['domainId'] = parseInt(splitArray[3]);
                                        m["Project"] = "way2target";
                                        console.log(splitArray);
                                    } else if(splitArray.length == 12)  {
                                        insertInDB = true;
                                        m['campId'] = parseInt(splitArray[2]);
                                        m['domainId'] = parseInt(splitArray[3]);
                                        m["Project"] = "way2target_selfservice";
                                    } else if(splitArray.length == 8){
                                        insertInDB = true;
                                        m['campId'] = parseInt(splitArray[2]);
                                        m['domainId'] = parseInt(splitArray[6]);
                                        m["Project"] = "P_Labs";
                                    }
                                }
                                m["sender"] = m["From"].split("<")[0].trim();
                                if(insertInDB) {
                                    connectToMongo.then((db)=>{
                                        db.collection("yahooTestData").insert(m).then((result)=>{
                                            console.log(result.result);
                                        }).catch((err)=>{

                                            if(err.code==11000){
                                                // console.error("Already Exited");
                                                // console.error(m);
                                            }else{
                                                console.error(err.stack);
                                            }

                                        })
                                    }).catch((err)=>{
                                        console.log(err);
                                    })
                                } else {
                                    console.log("not our mail id");
                                }
                            } else {
                                console.log(prefix + 'Body [%s] Finished', inspect(info.which));
                            }
                        });
                    });
                    msg.once('attributes', function(attrs) {
                        console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
                    });
                    msg.once('end', function() {
                        console.log(prefix + 'Finished');
                    });
                });
                f.once('error', function(err) {
                    console.log('Fetch error: ' + err);
                });
                f.once('end', function() {
                    console.log('Done fetching all messages in ' + box.name);
                    // console.log(seqNum);
                    if(box.name=='INBOX'){
                        openInbox('Bulk Mail',callBack);

                    }else{
                        setTimeout(function () {
                            openInbox('INBOX', callBack);
                        },60*1000)
                    }

                });
            },30*1000);

        }
    };
    imap.destroy();
    imap.connect();

    imap.once('ready', function() {
        if(imap.state != 'authenticated') {
            imap.connect();
        }
        console.log('[*] Listening For ',userDetails.user);
        openInbox('INBOX', callBack);
    });

    imap.once('error', function(err) {
        let options = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' ,hour12:true,hour:'2-digit',minute:'2-digit'};
        let dateTime = new Date(new Date().getTime()).toLocaleDateString('en-GB', options);
        let file = '\n' + err + '\n'+ dateTime + '\n' + userDetails.user + '\n' + imap.state + '\n';
        fs.appendFile('err-log.txt',file,(error) => {
            console.log(error);
        });
        if (err.Error == 'LOGIN Server error - Please try again later') {
            let imap = new Imap({
                user:userDetails.user,
                password:userDetails.password,
                host: 'imap.mail.yahoo.com',
                port: 993,
                tls: true,
                authTimeout: 5000
            });
            imap.connect();
        }
        if(imap.state != 'authenticated') {
            imap.connect();
        }
    });

    imap.once('end', function() {
        let options = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' ,hour12:true,hour:'2-digit',minute:'2-digit'};
        let dateTime = new Date(new Date().getTime()).toLocaleDateString('en-GB', options);
        let file = '\n' + 'Connection ended' + '\n'+ dateTime + '\n' + userDetails.user + '\n' + imap.state + '\n' ;
        fs.appendFile('err-log.txt',file,(error) => {
            console.log(error);
        });
        imap.connect();
    });
}